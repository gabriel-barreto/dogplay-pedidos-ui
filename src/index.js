import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

import "./assets/stylesheets/main.sass";

ReactDOM.render(<App />, document.getElementById("root"));
