import React from "react";

const Icon = ({ iconName, width, height, fill, ...other }) => (
    <svg
        fill={fill}
        width={width}
        height={height}
        dangerouslySetInnerHTML={{
            __html: `<use xlink:href="#${iconName}" preserveAspectRatio="xMidYMid meet" width=${width} height=${height} />`,
        }}
        version="1.1"
        preserveAspectRatio="xMidYMid meet"
        xmlns="http://www.w3.org/2000/svg"
        {...other}
    />
);

Icon.defaultProps = {
    fill: "#fff",
    width: "24px",
    height: "24px"
};

export default Icon;
