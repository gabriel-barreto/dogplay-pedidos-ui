import React from "react";

import "./index.sass";

const Card = ({ background, children }) => (
    <div className="card" style={{ backgroundColor: background }}>
        {children}
    </div>
);

Card.defaultProps = {
    background: "#fff",
};

export default Card;
