import React from "react";

import "./index.sass";

import Card from "../../components/Card";
import Icon from "../../components/Icon";

const Login = () => (
    <section className="login" id="login">
        <Card>
            <header className="header">
                <Icon
                    iconName="brand"
                    fill="#00274C"
                    width="106px"
                    height="24px"
                />
                <h2 className="title">Login</h2>
            </header>
            <form method="POST" onSubmit={() => {}} className="form">
                <div className="input-group">
                    <label htmlFor="email" className="label">
                        Email:
                    </label>
                    <input
                        type="email"
                        name="email"
                        id="email"
                        className="input"
                        placeholder="Exemplo: lorem@ipsum.com.br"
                        minLength="7"
                        required
                        autoFocus
                    />
                </div>
                <div className="input-group">
                    <label htmlFor="password" className="label">
                        Senha:
                    </label>
                    <input
                        name="password"
                        id="password"
                        type="password"
                        className="input"
                        placeholder="de 8 à 32 caracteres"
                        minLength="8"
                        maxLength="32"
                        required
                    />
                </div>
                <button className="btn -primary">Entrar</button>
                <button className="btn -transparent-primary">
                    Esqueceu a sua senha?
                </button>
            </form>
        </Card>
    </section>
);

export default Login;
